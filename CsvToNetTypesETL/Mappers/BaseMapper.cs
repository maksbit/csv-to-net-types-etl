﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CsvToNetTypesETL
{
    public abstract class BaseMapper
    {
        protected string GetValueByIndex(List<string> rowValues, int? index)
        {
            return index != null ? rowValues[index.Value] : null;
        }
    }
}
