﻿using System.Collections.Generic;

namespace CsvToNetTypesETL
{
    public class EnumMapper : BaseMapper
    {
        private readonly int nameIndex;
        private readonly int? descriptionIndex;

        public EnumMapper(int nameIndex, int? descriptionIndex)
        {
            this.nameIndex = nameIndex;
            this.descriptionIndex = descriptionIndex;
        }

        public string GetName(List<string> rowValues)
        {
            return GetValueByIndex(rowValues, nameIndex).TrimSafe();
        }

        public string GetDescription(List<string> rowValues)
        {
            return GetValueByIndex(rowValues, descriptionIndex).TrimSafe();
        }


    }
}
