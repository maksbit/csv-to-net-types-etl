﻿using System.Collections.Generic;

namespace CsvToNetTypesETL
{
    public class PropertyMapper : BaseMapper
    {
        private readonly int nameIndex;
        private readonly int typeIndex;
        private readonly int? descriptionIndex;
        private readonly int? validationIndex;
        private readonly int? occurenceIndex;

        public PropertyMapper(int nameIndex, int typeIndex, int? descriptionIndex, int? validationIndex, int? occurenceIndex)
        {
            this.nameIndex = nameIndex;
            this.typeIndex = typeIndex;
            this.descriptionIndex = descriptionIndex;
            this.validationIndex = validationIndex;
            this.occurenceIndex = occurenceIndex;
        }

        public string GetName(List<string> rowValues)
        {
            return GetValueByIndex(rowValues, nameIndex).TrimSafe();
        }

        public string GetType(List<string> rowValues)
        {
            return GetValueByIndex(rowValues, typeIndex).TrimSafe();
        }

        public string GetDescription(List<string> rowValues)
        {
            return GetValueByIndex(rowValues, descriptionIndex).TrimSafe();
        }

        public string GetValidation(List<string> rowValues)
        {
            return GetValueByIndex(rowValues, validationIndex).TrimSafe();
        }

        public string GetOccurence(List<string> rowValues)
        {
            return GetValueByIndex(rowValues, occurenceIndex).TrimSafe();
        }
    }
}
