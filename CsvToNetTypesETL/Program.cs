﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace CsvToNetTypesETL
{
    class Program
    {
        private static string defaultCsvFilePath = @"D:\api_error_contract.csv";

        static void Main(string[] args)
        {
            Settings.ConvertPropertyNameFromUnderscoreToCamelCase = true;

            Settings.PropertyMapper = new PropertyMapper(nameIndex: 0, typeIndex: 1, descriptionIndex: 2, validationIndex: null, occurenceIndex: 3);

            var allItemTypes = new List<TypeDescription>();

            var sourceFilePath = ParseCsvFile(allItemTypes);

            if (allItemTypes.Any() == false)
            {
                Logger.Log("no any types found in csv file");
                Console.ReadLine();
                return;
            }

            var targetDirPath = Path.GetDirectoryName(sourceFilePath);
            foreach (var item in allItemTypes)
            {
                var filePath = Path.Combine(targetDirPath, item.Name + ".cs");
                var contents = item.BuildText();
                if (!string.IsNullOrWhiteSpace(contents))
                {
                    File.WriteAllText(filePath, contents, Encoding.UTF8);
                }
            }

            Logger.Log("done");
            Console.ReadLine();
        }

        private static string ParseCsvFile(List<TypeDescription> allItemTypes)
        {
            StreamReader streamReader = null;
            string filePath = null;

            while (streamReader == null)
            {
                Logger.Log("paste file path to csv or Enter to exit");
                if (!string.IsNullOrWhiteSpace(defaultCsvFilePath))
                {
                    filePath = defaultCsvFilePath;
                    Logger.Log("default path used = " + defaultCsvFilePath);
                }
                else
                {
                    filePath = Console.ReadLine();
                }

                if (string.IsNullOrWhiteSpace(filePath))
                {
                    return null;
                }

                try
                {
                    streamReader = File.OpenText(filePath);
                }
                catch (Exception ex)
                {
                    Logger.Log("Error occured when tyring to read file: " + ex.Message);
                    filePath = null;
                    defaultCsvFilePath = null;
                }
            }



            using (streamReader)
            {
                var csvHelper = new CsvHelper.CsvReader(streamReader);

                TypeDescription item = null;

                while (csvHelper.Read())
                {
                    try
                    {
                        var rowValues = new List<string>();
                        for (int i = 0; csvHelper.TryGetField<string>(i, out string value); i++)
                        {
                            rowValues.Add(value);
                        }

                        if (rowValues.Count == 0 || rowValues.All(x => string.IsNullOrWhiteSpace(x)))
                        {
                            //empty row
                            continue;
                        }

                        //only first elemnt exists
                        if (!string.IsNullOrWhiteSpace(rowValues.First()) && rowValues.Skip(1).All(x => string.IsNullOrWhiteSpace(x)))
                        {
                            if ((item == null || string.IsNullOrWhiteSpace(item.Name) || (item.Properties != null || item.EnumValues != null)))
                            {
                                //new item starts
                                item = new TypeDescription();
                                allItemTypes.Add(item);

                                //type name
                                item.Name = rowValues.First().Replace(" ", "");
                            }
                            else
                            {
                                if (item.Properties == null && item.EnumValues == null)
                                {
                                    //several or sinlge line of description
                                    item.Description += rowValues.First();
                                }
                            }
                            continue;
                        }

                        if (rowValues.Select(x => x.Trim()).Any(x => x == "Description"))
                        {
                            //header row of table
                            continue;
                        }

                        //first 2 items exists => enum
                        if (!string.IsNullOrWhiteSpace(Settings.EnumMapper.GetName(rowValues)) 
                            && !string.IsNullOrWhiteSpace(Settings.EnumMapper.GetDescription(rowValues)) 
                            && rowValues.Skip(2).All(x => string.IsNullOrWhiteSpace(x)))
                        {
                            if (item.IsEnum == false)
                            {
                                item.IsEnum = true;
                                item.EnumValues = new List<EnumValueDescription>();
                            }
                            var value = new EnumValueDescription();
                            value.Name = Settings.EnumMapper.GetName(rowValues);
                            value.Description = Settings.EnumMapper.GetDescription(rowValues);

                            item.EnumValues.Add(value);
                        }
                        else
                        {
                            if (item.Properties == null)
                            {
                                item.Properties = new List<PropertyDescription>();
                            }

                            //verify new property exists
                            if (!string.IsNullOrWhiteSpace(Settings.PropertyMapper.GetName(rowValues)))
                            {
                                var value = new PropertyDescription();
                                value.Name = Settings.PropertyMapper.GetName(rowValues);
                                value.Type = Settings.PropertyMapper.GetType(rowValues);
                                value.Description = Settings.PropertyMapper.GetDescription(rowValues);
                                value.Validation = Settings.PropertyMapper.GetValidation(rowValues);
                                value.Occurence = Settings.PropertyMapper.GetOccurence(rowValues);
                                item.Properties.Add(value);
                            }
                            else
                            {
                                //else hack for excel merged cells
                                //just merge data from next row
                                var value = item.Properties.Last();
                                value.Type += Settings.PropertyMapper.GetType(rowValues);
                                value.Description += Settings.PropertyMapper.GetDescription(rowValues);
                                value.Validation += Settings.PropertyMapper.GetValidation(rowValues);
                                value.Occurence += Settings.PropertyMapper.GetOccurence(rowValues);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        csvHelper.TryGetField<string>(0, out string value);
                        Logger.Log("Failed to parse csv for type name = " + (item?.Name) + " csv row first value =" + value
                            + Environment.NewLine
                            + "Ex = " + ex);
                        return filePath;
                    }
                }

            }

            return filePath;
        }
    }
}
