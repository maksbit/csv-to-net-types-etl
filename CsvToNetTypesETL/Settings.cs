﻿namespace CsvToNetTypesETL
{
    public static class Settings
    {
        public static bool ConvertPropertyNameFromUnderscoreToCamelCase = true;

        public static PropertyMapper PropertyMapper = new PropertyMapper(0, 1, 2, 3, 4);
        public static EnumMapper EnumMapper = new EnumMapper(0, 1);
    }
}
