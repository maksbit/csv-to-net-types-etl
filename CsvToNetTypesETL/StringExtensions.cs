﻿using System;
using System.Xml;

namespace CsvToNetTypesETL
{
    public static class StringExtensions
    {
        public static string UnderScoreToCamelCase(this string name)
        {
            if (string.IsNullOrEmpty(name))
            {
                return name;
            }
            string[] array = name.Split('_');
            for (int i = 0; i < array.Length; i++)
            {
                string s = array[i];
                string first = string.Empty;
                string rest = string.Empty;
                if (s.Length > 0)
                {
                    first = Char.ToUpperInvariant(s[0]).ToString();
                }
                if (s.Length > 1)
                {
                    rest = s.Substring(1).ToLowerInvariant();
                }
                array[i] = first + rest;
            }
            string newname = string.Join("", array);
            //if (newname.Length > 0)
            //{
            //    newname = Char.ToLowerInvariant(newname[0]) + newname.Substring(1);
            //}
            //else
            //{
            //    newname = name;
            //}

            return newname;
        }

        public static string XmlEscape(this string unescaped)
        {
            XmlDocument doc = new XmlDocument();
            XmlNode node = doc.CreateElement("root");
            node.InnerText = unescaped;
            return node.InnerXml;
        }
    }
}
