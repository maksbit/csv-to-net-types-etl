﻿namespace CsvToNetTypesETL
{
    public static class StringHelperExtensions
    {
        public static string TrimSafe(this string value)
        {
            if (value == null)
                return null;

            return value.Trim();
        }
    }
}
