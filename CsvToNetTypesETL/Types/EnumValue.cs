﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CsvToNetTypesETL
{
    public class EnumValueDescription
    {
        private static string[] NetSpecialNames = new string[] { "return",
        "bool",
"Boolean",
"byte",
"Byte",
"sbyte",
"SByte",
"char",
"Char",
"decimal",
"Decimal",
"double",
"Double",
"float",
"Single",
"int",
"Int32",
"uint",
"UInt32",
"long",
"Int64",
"ulong",
"UInt64",
"object",
"Object",
"short",
"Int16",
"ushort",
"UInt16",
"string",
"String"};

        public string Name { get; set; }
        public string Description { get; set; }

        public void BuildText(StringBuilder sb)
        {
            //summary
            sb.Append(@"        /// <summary>
        /// ");
            sb.Append(Description.XmlEscape());
            sb.Append(@"
        /// </summary>
        ");

            if (NetSpecialNames.Contains(Name))
            {
                sb.Append("@");
            }
            sb.Append(Name);
            sb.Append(@",
");
        }
    }
}