﻿using System;
using System.Linq;
using System.Text;

namespace CsvToNetTypesETL
{
    public class PropertyDescription
    {
        public string Name { get; set; }
        public string Type { get; set; }
        public string Description { get; set; }
        public string Validation { get; set; }
        public string Occurence { get; set; }

        public bool IsNullable { get; set; }
        public bool IsArray { get; set; }

        private static string[] BasicTypes = new string[] { "int", "integer", "decimal", "float", "bool" };

        public void BuildText(StringBuilder sb)
        {
            //summary
            sb.Append(@"        /// <summary>
        /// ");
            sb.Append(Description.XmlEscape());

            sb.Append(@"
        /// Validation rules: ");
            sb.Append(Validation.XmlEscape());

            sb.Append(@"
        /// Occurence rules: ");
            sb.Append(Occurence.XmlEscape());

            sb.Append(@"
        /// </summary>
        ");

            if (Settings.ConvertPropertyNameFromUnderscoreToCamelCase)
            {
                sb.Append("[JsonProperty(PropertyName = \"");
                sb.Append(Name);
                sb.Append("\")");
                sb.Append(@"]
        ");
            }

            //validation 
            //dont know whether to apply attributes or json schema
            if (!string.IsNullOrWhiteSpace(Validation))
            {
                //add not valid attribute => dev should change it manually
                //if (Validation.Contains("Length"))
                //{
                //    result.Append("[StringLengh(,)]");
                //}
            }

            //dont know how it will be implemented but assume using validation attributes as well
            if (!string.IsNullOrWhiteSpace(Occurence))
            {
                //add not valid attribute => dev should change it manually
                var values = Occurence.Trim().Split("..", StringSplitOptions.RemoveEmptyEntries);
                var min = 0;
                var couldBeMany = false;
                if (values.Length > 0)
                {
                    min = int.Parse(values[0].Trim());
                }

                if (values.Length == 2)
                {
                    var maxStr = values[1].Trim();
                    couldBeMany = !string.IsNullOrWhiteSpace(maxStr) && maxStr != "1";
                }

                //    result.Append("[Occurence(min,max)]");

                //could be 0 occurences and single only and type=net type or custom enum
                if (min == 0 && !couldBeMany
                    && (BasicTypes.Any(x => x == Name) || Type.EndsWith("Type") || Type == "DateTimeOffset"))
                {
                    IsNullable = true;
                }

                if (couldBeMany)
                {
                    IsArray = true;
                }
            }


            sb.Append("public ");
            sb.Append(Type);
            if (IsArray)
            {
                sb.Append("[]");
            }
            if (IsNullable)
            {
                sb.Append("?");
            }
            sb.Append(" ");
            var name = Settings.ConvertPropertyNameFromUnderscoreToCamelCase ? Name.UnderScoreToCamelCase() : Name;
            sb.Append(name);
            sb.Append(@" { get; set; }
");
        }
    }
}
