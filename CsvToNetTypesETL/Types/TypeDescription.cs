﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CsvToNetTypesETL
{
    public class TypeDescription
    {
        public string Name { get; set; }
        public string Description { get; set; }

        public bool IsEnum { get; set; }
        public List<PropertyDescription> Properties { get; set; }

        public List<EnumValueDescription> EnumValues { get; set; }

        public string BuildText()
        {
            if (Properties == null && EnumValues == null)
                return null;

            var sb = new StringBuilder();
            
            sb.Append(@"using System;");
            if (Settings.ConvertPropertyNameFromUnderscoreToCamelCase)
            {
                sb.AppendLine().Append(@"using Newtonsoft.Json;");
            }

            sb.Append(@"

namespace CsvToNetTypesETL
{
    ");

            if (!string.IsNullOrWhiteSpace(Description))
            {
                //summary
                sb.Append(@"/// <summary>
    /// ");
                sb.Append(Description.XmlEscape());
                sb.Append(@"
    /// </summary>");
            }

            sb.Append(@"
    public ");
            sb.Append(IsEnum ? "enum" : "class");
            sb.Append(" ");
            sb.Append(Name);
            sb.Append(@"
    {
"); ;
            if (IsEnum)
            {
                EnumValues.ForEach(x => x.BuildText(sb));
            }
            else
            {
                Properties.ForEach(x => x.BuildText(sb));
            }

            sb.Append(@"    }
}");

            return sb.ToString();
        }
    }



}
